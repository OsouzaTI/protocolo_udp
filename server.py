import socket

class UDPServer:

    def __init__(self, localIp, port, bufferSize) -> None:        
        self.bufferSize = bufferSize
        self.UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.UDPServerSocket.bind((localIp, port))
        print(f'Servidor rodando em {localIp}:{port}')

    def run(self):
        while True:
            bytesAddressPair = self.UDPServerSocket.recvfrom(self.bufferSize)
            message, address = bytesAddressPair[0], bytesAddressPair[1]
            self.clientMessage(f'{message} | {address}')
            self.sendToClient(address, 'Ola cliente como vai ?')

    def clientMessage(self, message):
        print(f'[Server] {message}')

    def sendToClient(self, address, message):
        self.UDPServerSocket.sendto(str.encode(message), address)

server = UDPServer('127.0.0.1', 20001, 1024)
server.run()