import socket
from time import sleep

class UDPClient:

    def __init__(self, host, port, bufferSize) -> None:
        self.serverAddress = (host, port)
        self.bufferSize = bufferSize
        self.UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    
    def sendMessage(self, message):
        self.UDPClientSocket.sendto(str.encode(message), self.serverAddress)
        serverMessage = self.UDPClientSocket.recvfrom(self.bufferSize)
        print(f'[Client] Server say: {serverMessage[0]}')

client = UDPClient('127.0.0.1', 20001, 1024)

while True:
    client.sendMessage(f'Ola mundo')
    sleep(2)